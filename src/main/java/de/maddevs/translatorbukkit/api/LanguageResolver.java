package de.maddevs.translatorbukkit.api;

import de.maddevs.translator.api.Translator;
import de.maddevs.translator.core.Language;
import de.maddevs.translatorbukkit.util.ReflectionUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * <h1>LanguageResolver</h1>
 * <p>
 * The LanguageResolver class can get the selected local from
 * a player. The method LanguageResolver.lang(CommandSender); can be
 * used to get a non nullable Language object from a command sender.
 *
 * @author Drayke
 * @version 1.0
 * @see LanguageResolver#lang(CommandSender) LanguageResolver#lang(CommandSender)
 * @see Language
 * @since 30.07.2017
 */
public final class LanguageResolver
{

    /**
     * Gets the Language object for a CommandSender.
     * <ol>
     * <li>If the CommandSender is a Player, his currently selected local will be resolved</li>
     * <li>If this procedure fails, the default language of the Dictionary will be used</li>
     * </ol>
     *
     * @param sender the sender
     *
     * @return the language object (should never be null)
     *
     * @see Language
     * @see Translator#getDefaultLanguage();
     */
    public static Language lang( CommandSender sender )
    {
        try
        {
            if ( !( sender instanceof Player ) )
            {
                return Translator.getDefaultLanguage();
            }

            String playerLanguage = ReflectionUtil.getPlayerLanguage( (Player) sender );
            Language language = Translator.getDictionary().getLanguage( playerLanguage );
            if ( language != null ) return language;
            else return Translator.getDefaultLanguage();
        }
        catch ( Exception e )
        {
            return Translator.getDefaultLanguage();
        }
    }

    /**
     * Gets the minecraft language.
     *
     * @param player the player
     *
     * @return the minecraft language
     *
     * @see ReflectionUtil#getPlayerMinecraftLanguage(Player)
     */
    public static String getMinecraftLanguage( Player player )
    {
        try
        {
            return ReflectionUtil.getPlayerMinecraftLanguage( player );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        return null;
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/