package de.maddevs.translatorbukkit;


import de.maddevs.translator.api.Translator;
import de.maddevs.translator.core.Dictionary;
import de.maddevs.translator.core.DictionaryFile;
import de.maddevs.translatorbukkit.command.TranslatorCommand;
import de.maddevs.translatorbukkit.converter.EConverter;
import de.maddevs.translatorbukkit.manager.DictionaryLoader;
import de.maddevs.translatorbukkit.util.Messages;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * <h1>The TranslatorPlugin.</h1>
 * <p>
 * First called class by Bukkit classloader.
 *
 * @author Drayke
 * @version 1.0
 * @since 23.07.2017
 */
public class TranslatorPlugin extends JavaPlugin
{

    /**
     * Delay after plugin enabled.
     */
    public static final long POST_DELAY = 3L;

    @Getter
    private static TranslatorPlugin instance;

    private Dictionary dictionary;

    @Override
    public void onEnable()
    {
        instance = this;
        dictionary = new Dictionary();
        Translator.setDictionary( dictionary );

        //Register the commands (see CommandAPI)
        new TranslatorCommand().register( this );
        //Init and register default Argument Converters
        EConverter.init();
        //Register translation file
        Translator.getDictionary().registerDictionaryFile( new DictionaryFile( getResource( "dictionary.xml" ) ) );

        Messages.send( Bukkit.getConsoleSender(), "Version: §e" + getInstance().getDescription().getVersion() );

        //Post enable
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                //Load external dictionary files
                DictionaryLoader.loadDictionaryFiles();
            }
        }.runTaskLater( this, POST_DELAY );
    }

    public void console( String... messages )
    {
        for ( String msg : messages )
        {
            Bukkit.getConsoleSender().sendMessage( ChatColor.translateAlternateColorCodes( '&', msg ) );
        }
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/