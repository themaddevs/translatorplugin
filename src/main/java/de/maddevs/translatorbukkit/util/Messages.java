package de.maddevs.translatorbukkit.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * <h1>Formatted messages</h1>
 * <p>
 * Just a little helper class for easy logging and
 * player messages
 * </p>
 *
 * @author Drayke
 * @version 0.1
 * @since 30.06.2017
 */
public final class Messages
{

    /**
     * Prefix for messages
     */
    private static final String pre = "§9[§eTranslatorAPI§9] §7";

    /**
     * Prints to console
     *
     * @param message the message
     */
    public static void log( String... message )
    {
        send( Bukkit.getConsoleSender(), message );
    }

    /**
     * Sends to a CommandSender
     *
     * @param sender  the sender
     * @param message the message
     */
    public static void send( CommandSender sender, String... message )
    {

        if ( message.length > 1 ) sender.sendMessage( pre );
        else message[0] = pre + message[0];

        for ( String msg : message )
        {
            msg = ChatColor.translateAlternateColorCodes( '&', msg );
            sender.sendMessage( msg );
        }

    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/