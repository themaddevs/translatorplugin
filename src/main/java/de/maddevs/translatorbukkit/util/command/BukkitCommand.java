package de.maddevs.translatorbukkit.util.command;

import de.maddevs.command.ACommand;
import de.maddevs.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;

import static de.maddevs.translatorbukkit.util.command.Implicits.COMMANDSENDER;
import static de.maddevs.translatorbukkit.util.command.Implicits.PLAYER;

/**
 * <h1>BukkitCommand</h1>
 *
 * @author Juyas
 * @version 1.0
 * @since 01.12.2017
 */
public abstract class BukkitCommand<Plugin extends JavaPlugin> extends ACommand<Plugin> implements CommandExecutor, TabCompleter
{

    protected BukkitCommand( Class<? extends ACommand<Plugin>> thisClass, String[] validHelpTags )
    {
        super( thisClass, validHelpTags );
    }

    protected BukkitCommand( Class<? extends ACommand<Plugin>> thisClass, ACommand<Plugin>... subCommands )
    {
        super( thisClass, subCommands );
    }

    @Override
    public boolean onCommand( CommandSender sender, org.bukkit.command.Command command, String label, String[] args )
    {
        onCommand( new BukkitCommandExecutor( sender ), prepateImplicits( sender ), command.getName(), args );
        return true;
    }

    @Override
    public List<String> onTabComplete( CommandSender sender, org.bukkit.command.Command command, String alias, String[] args )
    {
        return onTabComplete( new BukkitCommandExecutor( sender ), command.getName(), args );
    }

    private Implicits prepateImplicits( CommandSender sender )
    {
        Implicits implicit = new Implicits();
        implicit.setValue( COMMANDSENDER, sender );
        if ( sender instanceof Player )
        {
            Player player = (Player) sender;
            implicit.setValue( PLAYER, player );
        }
        return implicit;
    }

    public void register( Plugin plugin )
    {
        filterCommands();

        for ( Command c : getCommandAnnotations() )
        {

            PluginCommand command = plugin.getCommand( c.name() );
            if ( command != null )
            {
                if ( !c.permission().isEmpty() )
                    command.setPermission( c.permission() );
                if ( c.aliases().length > 0 )
                    command.setAliases( Arrays.asList( c.aliases() ) );
                command.setExecutor( this );
                command.setTabCompleter( this );
            }
        }
        setValidator( new BukkitArgumentValidator() );

    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/