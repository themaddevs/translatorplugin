package de.maddevs.translatorbukkit.util.command;

import de.maddevs.command.ImplicitReceiver;

/**
 * <h1>Implicits</h1>
 *
 * @author Juyas
 * @version 1.0
 * @since 07.12.2017
 */
public class Implicits implements ImplicitReceiver
{

    public static final int PLAYER = 0;

    public static final int COMMANDSENDER = 1;

    private Object[] values = new Object[2];

    public void setValue( int id, Object value )
    {
        values[id] = value;
    }

    @Override
    public Object getImplicitValue( int i )
    {
        if ( i < 0 || i >= values.length ) return null;
        return values[i];
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/