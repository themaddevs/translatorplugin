package de.maddevs.translatorbukkit.util.command;

import de.maddevs.command.IArgumentValidator;

/**
 * <h1>BukkitArgumentValidator</h1>
 *
 * @author Juyas
 * @version 1.0
 * @since 03.12.2017
 */
public class BukkitArgumentValidator implements IArgumentValidator
{

    @Override
    public boolean validate( Object[] objects, int[] ints )
    {
        for ( int i = 0; i < Math.min( objects.length, ints.length ); i++ )
        {
            if ( !validate( objects[i], ints[i] ) ) return false;
        }
        return true;
    }

    private boolean validate( Object object, int note )
    {
        if ( note != IArgumentValidator.NONE && object == null ) return false;
        return true;
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/