package de.maddevs.translatorbukkit.util.command;

import de.maddevs.command.ICommandExecutor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.command.CommandSender;

/**
 * <h1>BukkitCommandExecutor</h1>
 *
 * @author Juyas
 * @version 1.0
 * @since 01.12.2017
 */
@AllArgsConstructor
public final class BukkitCommandExecutor implements ICommandExecutor
{

    @Getter
    private final CommandSender commandSender;

    @Override
    public void sendMessage( String message )
    {
        commandSender.sendMessage( message );
    }

    @Override
    public boolean hasPermission( String perm )
    {
        return commandSender.hasPermission( perm );
    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/