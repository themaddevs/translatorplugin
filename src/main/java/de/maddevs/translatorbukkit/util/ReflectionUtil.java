package de.maddevs.translatorbukkit.util;

import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * <h1>ReflectionUtil</h1>
 * Some Reflection stuff to get the local of a minecraft client
 *
 * @author Drayke
 * @version 1.0
 * @since 30.07.2017
 */
public final class ReflectionUtil
{

    /**
     * Gets player language key usable in the dictionary.
     * The language code from getPlayerMinecraftLanguage() is just
     * reduced to the language code.
     * "de_DE" to "de"
     *
     * @param player the player
     *
     * @return the player language
     *
     * @throws Exception an exception while reflection
     */
    public static String getPlayerLanguage( Player player ) throws Exception
    {
        String lang = getPlayerMinecraftLanguage( player );
        lang = lang.split( "_" )[0];
        return lang;
    }


    /**
     * Gets current minecraft language/local of a player.
     * The return value looks like: "de_DE"
     *
     * @param player the player
     *
     * @return the player minecraft language
     *
     * @throws Exception an exception while reflection
     */
    public static String getPlayerMinecraftLanguage( Player player ) throws Exception
    {
        Class entityPlayerClass = PackageType.MINECRAFT_SERVER.getClass( "EntityPlayer" );
        Class craftPlayerClass = PackageType.CRAFTBUKKIT_ENTITY.getClass( "CraftPlayer" );
        Method handle = craftPlayerClass.getDeclaredMethod( "getHandle" );
        Object entityPlayer = handle.invoke( craftPlayerClass.cast( player ) );
        Field locale = entityPlayerClass.getDeclaredField( "locale" );
        String lang = (String) locale.get( entityPlayer );
        return lang;

    }

}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/