package de.maddevs.translatorbukkit.converter;

import de.maddevs.translator.api.IArgumentConverter;
import de.maddevs.translator.api.Translator;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <h1>Container for all Converter</h1>
 * <p>
 * This enum just initializes all converters within this enum.
 *
 * @author Drayke
 * @version 1.0
 * @since 23.07.2017
 */

@AllArgsConstructor
public enum EConverter
{
    LANGUAGE( new LanguageConverter() ),
    LOCATION( new LocationConverter() ),
    PLAYER( new PlayerConverter() ),
    MATERIAL( new MaterialConverter() ),
    WORLD( new WorldConverter() );

    @Getter
    private IArgumentConverter argumentConverter;

    /**
     * Adds the default IArguments to the Translator.
     *
     * @see Translator#registerConverter(Class, IArgumentConverter)
     */
    public static void init()
    {
        for ( EConverter converter : values() )
        {
            Translator.registerConverter( converter.getArgumentConverter().getType(), converter.getArgumentConverter() );
        }
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/