package de.maddevs.translatorbukkit.converter;

import de.maddevs.translator.api.IArgumentConverter;
import org.bukkit.Location;

/**
 * <h1>LocationConverter</h1>
 * <p>
 * Converts bukkit Location object to String
 * like: World: $world_name$(X: $location_x$, Y: $location_y$, Z: $location_z$)
 *
 * @author Drayke
 * @version 1.0
 * @see Location
 * @since 23.07.2017
 */
public class LocationConverter implements IArgumentConverter<Location>
{

    @Override
    public String convert( Location location )
    {
        return "World: " + location.getWorld().getName() + "(X: " + location.getBlockX() + ",Y: " + location.getBlockY() + ",Z: " + location.getBlockZ() + ")";
    }

    @Override
    public Class<Location> getType()
    {
        return Location.class;
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/