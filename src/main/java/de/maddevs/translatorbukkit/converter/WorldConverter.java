package de.maddevs.translatorbukkit.converter;

import de.maddevs.translator.api.IArgumentConverter;
import org.bukkit.World;

/**
 * <h1>WorldConverter</h1>
 * [The WorldConverter description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 31.10.2017
 */
public class WorldConverter implements IArgumentConverter<World>
{

    @Override
    public String convert( World world )
    {
        return world.getName();
    }

    @Override
    public Class<World> getType()
    {
        return World.class;
    }
}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/