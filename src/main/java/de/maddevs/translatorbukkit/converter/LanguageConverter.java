package de.maddevs.translatorbukkit.converter;

import de.maddevs.translator.api.IArgumentConverter;
import de.maddevs.translator.core.Language;

/**
 * <h1>LanguageConverter</h1>
 * <p>
 * Converts Language object to String
 * like: $language_name$ ("$language_code$")
 *
 * @author Drayke
 * @version 1.0
 * @see Language
 * @since 23.07.2017
 */
public class LanguageConverter implements IArgumentConverter<Language>
{

    @Override
    public String convert( Language language )
    {
        return language.getLanguageName() + " (" + language.getLanguageKey() + ")";
    }

    @Override
    public Class<Language> getType()
    {
        return Language.class;
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/