package de.maddevs.translatorbukkit.converter;

import de.maddevs.translator.api.IArgumentConverter;
import org.bukkit.entity.Player;

/**
 * <h1>PlayerConverter</h1>
 * <p>
 * Converts bukkit Player object into String
 * like: $player_displayname$
 *
 * @author Drayke
 * @version 1.0
 * @see Player
 * @since 23.07.2017
 */
public class PlayerConverter implements IArgumentConverter<Player>
{

    @Override
    public String convert( Player player )
    {
        return player.getName();
    }

    @Override
    public Class<Player> getType()
    {
        return Player.class;
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/