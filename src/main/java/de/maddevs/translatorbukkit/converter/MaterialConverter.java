package de.maddevs.translatorbukkit.converter;

import de.maddevs.translator.api.IArgumentConverter;
import org.bukkit.Material;

/**
 * <h1>MaterialConverter</h1>
 * [The MaterialConverter description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 31.10.2017
 */
public class MaterialConverter implements IArgumentConverter<Material>
{

    @Override
    public String convert( Material material )
    {
        return material.name() + ":" + material.getId();
    }

    @Override
    public Class<Material> getType()
    {
        return Material.class;
    }
}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/