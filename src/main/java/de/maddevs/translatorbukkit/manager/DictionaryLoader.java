package de.maddevs.translatorbukkit.manager;

import de.maddevs.translator.api.Translator;
import de.maddevs.translator.core.DictionaryFile;
import de.maddevs.translatorbukkit.TranslatorPlugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * <h1>DictionaryLoader</h1>
 * [The DictionaryLoader description comes here]
 *
 * @author Drayke
 * @version 1.0
 * @since 07.10.2017
 */
public class DictionaryLoader
{

    public static void loadDictionaryFiles()
    {
        File dataFolder = TranslatorPlugin.getInstance().getDataFolder();
        if ( !dataFolder.exists() && !dataFolder.mkdir() ) return;
        File[] files = dataFolder.listFiles( ( dir, name ) -> name.endsWith( ".xml" ) );
        try
        {
            for ( File file : files )
            {

                DictionaryFile dictFile = new DictionaryFile( new FileInputStream( file ) );
                TranslatorPlugin.getInstance().console( "§9Loaded file: §e" + file.getName() );
                Translator.getDictionary().registerDictionaryFile( dictFile, ( languageDuplicates, translationDuplicates, merged ) -> {
                    if ( languageDuplicates > 0 )
                        TranslatorPlugin.getInstance().console( "§9Language duplicates: " + languageDuplicates );
                    if ( translationDuplicates > 0 )
                        TranslatorPlugin.getInstance().console( "§9Original duplicates: " + translationDuplicates );
                    if ( merged > 0 )
                        TranslatorPlugin.getInstance().console( "§9Merged translations texts: " + merged );
                } );
            }
        }
        catch ( FileNotFoundException e )
        {
            e.printStackTrace();
        }

    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/