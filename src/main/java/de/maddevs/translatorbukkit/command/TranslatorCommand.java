package de.maddevs.translatorbukkit.command;


import de.maddevs.command.Command;
import de.maddevs.translatorbukkit.TranslatorPlugin;
import de.maddevs.translatorbukkit.util.command.BukkitCommand;

/**
 * <h1>The translator main command</h1>
 *
 * @author Drayke
 * @version 1.0
 * @since 30.06.2017
 */
public class TranslatorCommand extends BukkitCommand<TranslatorPlugin>
{

    public TranslatorCommand()
    {
        super( TranslatorCommand.class );
        registerSubCommand( new TranslatorSubCommands() );
    }

    @Command(name = "translator", aliases = "tr")
    public void empty()
    {

    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/