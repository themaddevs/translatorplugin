package de.maddevs.translatorbukkit.command;

import de.maddevs.command.Command;
import de.maddevs.command.ICommandExecutor;
import de.maddevs.command.ICommandTabCompleter;
import de.maddevs.translator.api.Translator;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>TranslatorTabCompleter</h1>
 * <p>
 * Tabcompleter for the Translator command
 * </p>
 *
 * @author Drayke
 * @version 1.0
 * @since 30.07.2017
 */
@NoArgsConstructor
public class TranslatorTabCompleter implements ICommandTabCompleter
{

    @Override
    public List<String> complete( ICommandExecutor executor, Class aClass, Command command, String s, Class aClass1, String[] strings )
    {
        if ( s.equals( "langKey" ) )
        {
            List<String> list = new ArrayList<>();
            Translator.getDictionary().getLanguages().forEach( language -> list.add( language.getLanguageKey() ) );
            return list;
        }
        return null;
    }
}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/