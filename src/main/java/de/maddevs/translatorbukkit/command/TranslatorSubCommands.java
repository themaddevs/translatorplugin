package de.maddevs.translatorbukkit.command;

import de.maddevs.command.*;
import de.maddevs.translator.api.Translator;
import de.maddevs.translator.core.Language;
import de.maddevs.translatorbukkit.TranslatorPlugin;
import de.maddevs.translatorbukkit.api.LanguageResolver;
import de.maddevs.translatorbukkit.util.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static de.maddevs.translator.api.Translator.tr;
import static de.maddevs.translatorbukkit.api.LanguageResolver.lang;
import static de.maddevs.translatorbukkit.util.command.Implicits.COMMANDSENDER;
import static de.maddevs.translatorbukkit.util.command.Implicits.PLAYER;


/**
 * <h1>Sub commands for translator</h1>
 * <p>
 * All sub commands for the main translator command.
 * </p>
 *
 * @author Juyas, Drayke
 * @version 0.1
 * @since 21.06.2017
 */
public final class TranslatorSubCommands extends ACommand<TranslatorPlugin>
{

    protected TranslatorSubCommands()
    {
        super( TranslatorSubCommands.class );
    }


    @HelpPage(desc = "Prints your current selected language in minecraft.", auto = true)
    @Command(name = "local", validArguments = 0)
    public void myLanguage( @Implicit(impl = PLAYER) Player player )
    {
        try
        {
            Language language = lang( player );
            if ( language == null )
                Messages.send( player, tr( "§aYour current selected minecraft local is:§e ## §cbut it couldn't be resolved to a supported language.", LanguageResolver.getMinecraftLanguage( player ) ) );
            else Messages.send( player, tr( language, "§aYour current selected language is:§e ##", language ) );
        }
        catch ( Exception e )
        {
            Messages.send( player, tr( "§cYour current minecraft local couldn't be resolved!" ) );
            e.printStackTrace();
        }

    }

    @HelpPage(desc = "Lists information about the current dictionary.", auto = true)
    @Command(name = "info", validArguments = 0, permission = "translator.info")
    public void info( @Implicit(impl = COMMANDSENDER) CommandSender sender )
    {
        int translations = Translator.getDictionary().getEntries().size();
        int languages = Translator.getDictionary().getLanguages().size();
        Messages.send( sender, tr( lang( sender ), "§7Default Language:§e ## §7, Translations:§e ##§7, Languages:§e ##", Translator.getDefaultLanguage(), Integer.toString( translations ), Integer.toString( languages ) ) );

    }

    @HelpPage(desc = "Lists available (registered) languages of the dictionary.", auto = true)
    @Command(name = "languages", aliases = "lang", validArguments = 0, permission = "translator.info")
    public void languages( @Implicit(impl = COMMANDSENDER) CommandSender sender )
    {
        Messages.send( sender, tr( lang( sender ), "Dictionary Languages:" ) );
        for ( Language language : Translator.getDictionary().getLanguages() )
        {
            sender.sendMessage( "§8" + language.getLanguageName() + "§7(§e" + language.getLanguageKey() + "§7)" );
        }
    }

    @HelpPage(desc = "Gets the Languge name for a language key", auto = true)
    @Command(name = "lang", aliases = "getlang", validArguments = 1, permission = "translator.info", tab = TranslatorTabCompleter.class)
    public void getLanguageName( @Implicit(impl = COMMANDSENDER) CommandSender sender, @Arg(name = "langKey") String key )
    {
        if ( Translator.getDictionary().hasLanguage( key ) )
        {
            Language language = Translator.getDictionary().getLanguage( key );
            Messages.send( sender, tr( lang( sender ), "§aFound language:§e ##", language.getLanguageName() ) );
        }
        else
        {
            Messages.send( sender, tr( lang( sender ), "§cNo language found for: §e##", key ) );
        }
    }


}

/***********************************************************************************************
 *
 *                  All rights reserved, MadDevs (c) copyright 2017
 *
 ***********************************************************************************************/